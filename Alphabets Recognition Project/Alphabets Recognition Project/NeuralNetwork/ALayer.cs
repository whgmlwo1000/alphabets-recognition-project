﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alphabets_Recognition_Project
{
    abstract class ALayer
    {
        public double[] Totals { get { return mTotals; } }
        public double[] Outputs { get { return mOutputs; } }
        public double[] Biases { get { return mBiases; } protected set { mBiases = value; } }
        public double[,] Weights { get { return mWeights; } protected set { mWeights = value; } }

        public int CountOfPrevUnits { get { return mCountOfPrevUnits; } }
        public int CountOfCurUnits { get { return mCountOfCurUnits; } }

        // 각 유닛의 전체 합
        private double[] mTotals;
        // 각 유닛의 바이어스
        private double[] mBiases;
        // 각 유닛의 전체합과 바이어스를 더한 값을 입력으로 한 활성화함수 출력 값
        private double[] mOutputs;
        // 이전 레이어와 현재 레이어의 각 유닛들이 연결되어 형성되는 가중치
        private double[,] mWeights;

        // 이전 레이어의 유닛 개수
        private int mCountOfPrevUnits;
        // 현재 레이어의 유닛 개수
        private int mCountOfCurUnits;

        public ALayer(int countOfPrevUnits, int countOfCurUnits)
        {
            mCountOfPrevUnits = countOfPrevUnits;
            mCountOfCurUnits = countOfCurUnits;
            mTotals = new double[mCountOfCurUnits];
            mOutputs = new double[mCountOfCurUnits];
        }

        // 시뮬레이트 함수 / 실행함수
        public abstract void Simulate(ALayer prevLayer);
        // 활성화함수
        public abstract double Activate(double input);
        // 활성화함수 미분
        public abstract double DeltaActivate(double input);
    }
}
