﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alphabets_Recognition_Project.NeuralNetwork
{
    enum EInitWays
    {
        CONSTANT, RANDOM
    }
    abstract class AConvolutionLayer : ALayer
    {
        public int FilterWidth { get { return mFilterWidth; } }
        public int FilterHeight { get { return mFilterHeight; } }
        public int Stride { get { return mStride; } }
        public int CountOfFilters { get { return mCountOfFilters; } }
        public int CountOfChannels { get { return mCountOfChannels; } }
        public int PrevImgWidth { get { return mPrevImgWidth; } }
        public int PrevImgHeight { get { return mPrevImgHeight; } }
        public int CurImgWidth { get { return mCurImgWidth; } }
        public int CurImgHeight { get { return mCurImgHeight; } }
        public int Padding { get { return mPadding; } }


        // Filter의 가로 크기
        private int mFilterWidth;
        // Filter의 세로 크기
        private int mFilterHeight;
        // Filter의 개수
        private int mCountOfFilters;
        // 이미지 채널의 수
        private int mCountOfChannels;
        // 이전 레이어 이미지 가로 크기
        private int mPrevImgWidth;
        // 이전 레이어 이미지 세로 크기
        private int mPrevImgHeight;
        // Stride 크기
        private int mStride;
        // 현재 레이어 이미지 가로 크기
        private int mCurImgWidth;
        // 현재 레이어 이미지 세로 크기
        private int mCurImgHeight;
        // 현재 레이어 이미지 패딩 값
        private int mPadding;

        /// <summary>
        /// Convolution Layer 생성자
        /// </summary>
        /// <param name="prevImgWidth">이전 레이어의 Padding을 포함한 이미지 가로 크기</param>
        /// <param name="prevImgHeight">이전 레이어의 Padding을 포함한 이미지 세로 크기</param>
        /// <param name="countOfChannels">이미지의 채널 수</param>
        /// <param name="filterWidth">Filter의 가로 크기</param>
        /// <param name="filterHeight">Filter의 세로 크기</param>
        /// <param name="countOfFilters">Filter의 수</param>
        /// <param name="padding">현재 레이어의 Padding</param>
        /// <param name="stride">Filter의 Stride</param>
        /// <param name="filterInitWay">Filter를 초기화하는 방법</param>
        /// <param name="filterValue">Filter의 초기값</param>
        /// <param name="biasValue">Bias의 초기값</param>
        public AConvolutionLayer(int prevImgWidth, int prevImgHeight, int countOfChannels,
            int filterWidth, int filterHeight, int countOfFilters, int padding, int stride, 
            EInitWays filterInitWay, double filterValue, double biasValue)
            : base(prevImgWidth * prevImgHeight, ((prevImgWidth - filterWidth) / stride + 1 + padding * 2) * ((prevImgHeight - filterHeight) / stride + 1 + padding * 2) * countOfChannels * countOfFilters) 
        {
            mPrevImgWidth = prevImgWidth;
            mPrevImgHeight = prevImgHeight;
            mCountOfChannels = countOfChannels;
            mFilterWidth = filterWidth;
            mFilterHeight = filterHeight;
            mCountOfFilters = countOfFilters;
            mPadding = padding;
            mStride = stride;
            mCurImgWidth = (mPrevImgWidth - mFilterWidth) / mStride + 1 + mPadding * 2;
            mCurImgHeight = (mPrevImgHeight - mFilterHeight) / mStride + 1 + mPadding * 2;

            Weights = new double[mCountOfFilters, mCountOfChannels * mFilterWidth * mFilterHeight];
            Biases = new double[mCountOfFilters];

            switch (filterInitWay)
            {
                case EInitWays.RANDOM:
                    Random rand = new Random();
                    for (int i = 0; i < mCountOfFilters; i++)
                    {
                        for (int j = 0; j < mCountOfChannels * mFilterWidth * mFilterHeight; j++)
                        {
                            Weights[i, j] = rand.NextDouble() * filterValue * 2.0 - filterValue;
                        }
                        Biases[i] = rand.NextDouble() * biasValue * 2.0 - biasValue;
                    }
                    break;
                case EInitWays.CONSTANT:
                    for (int i = 0; i < mCountOfFilters; i++)
                    {
                        for (int j = 0; j < mCountOfChannels * mFilterWidth * mFilterHeight; j++)
                        {
                            Weights[j, i] = filterValue;
                        }
                        Biases[i] = biasValue;
                    }
                    break;
            }
        }

        public sealed override void Simulate(ALayer prevLayer)
        {
            double sum;

            for (int filter = 0; filter < mCountOfFilters; filter++)
            {
                for (int channel = 0; channel < mCountOfChannels; channel++)
                {
                    for (int prevRow = 0; prevRow < PrevImgHeight; prevRow += mStride)
                    {
                        for (int prevColumn = 0; prevColumn < PrevImgWidth; prevColumn += mStride)
                        {
                            sum = 0.0;
                            for(int filterRow = 0; filterRow < mFilterHeight; filterRow++)
                            {
                                for(int filterColumn = 0; filterColumn < mFilterWidth; filterColumn++)
                                {
                                    sum += prevLayer.Outputs[filter * mCountOfChannels * CurImgHeight * CurImgWidth + channel * PrevImgHeight * PrevImgWidth 
                                        + (prevRow + filterRow) * PrevImgWidth + (prevColumn + filterColumn)]
                                        * Weights[filter, channel * mFilterHeight * mFilterWidth + filterRow * mFilterWidth + filterColumn];
                                }
                            }
                            sum += Biases[filter];
                            int curIndex = mPadding * CurImgWidth + mPadding + filter * mCountOfChannels * CurImgHeight * CurImgWidth + channel * CurImgHeight * CurImgWidth
                                + prevRow / mStride * CurImgWidth + prevColumn / mStride;
                            Totals[curIndex] = sum;
                            Outputs[curIndex] = Activate(sum);
                        }
                    }
                }
            }
        }
    }
}
