﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alphabets_Recognition_Project.NeuralNetwork
{
    class FcSigmoidLayer : AFcLayer
    {
        public FcSigmoidLayer(int countOfPrevUnits, int countOfCurUnits) : base(countOfPrevUnits, countOfCurUnits)
        {

        }

        public override double Activate(double input)
        {
            return 1.0 / (1.0 + Math.Exp(-input));
        }

        public override double DeltaActivate(double input)
        {
            return Activate(input) * (1.0f - Activate(input));
        }
    }
}
