﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alphabets_Recognition_Project.NeuralNetwork
{
    class FcIdentityFunctionLayer : AFcLayer
    {
        public FcIdentityFunctionLayer(int countOfPrevUnits, int countOfCurUnits) : base(countOfPrevUnits, countOfCurUnits)
        {

        }
        public override double Activate(double input)
        {
            return input;
        }
        public override double DeltaActivate(double input)
        {
            return 1.0;
        }
    }
}
